﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlTemplates.Utilities;
using Xamarin.Forms;

namespace ControlTemplates
{
    public partial class MainPage : NavigationButtons
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.PageTitle = "Main";
        }

        public void View1_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new PageOne());
        }

        public void View2_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new PageTwo());
        }

        public void View3_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new PageThree());
        }

        public void View4_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new PageFour());
        }
    }
}
