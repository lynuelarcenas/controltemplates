﻿using System;
using System.Collections.Generic;
using ControlTemplates.Utilities;
using Xamarin.Forms;

namespace ControlTemplates
{
    public partial class PageTwo : NavigationButtons
    {
        public PageTwo()
        {
            InitializeComponent();
            InitializeNavigation();
            NavigationPage.SetHasNavigationBar(this, false);

        }

        private void InitializeNavigation()
        {
            this.PageTitle = "View 2";
            this.LeftIcon = "BackArrow";
            this.RightIcon2 = "Delete";
            this.LeftButtonCommand = new Command((obj) => Navigation.PopAsync());
            this.RightButton2Command = new Command((obj) => DisplayAlert("Delete", "This button is for delete function", "Okay"));
        }
    }
}
