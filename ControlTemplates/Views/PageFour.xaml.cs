﻿using System;
using System.Collections.Generic;
using ControlTemplates.Utilities;
using Xamarin.Forms;

namespace ControlTemplates
{
    public partial class PageFour : NavigationButtons
    {
        public PageFour()
        {
            InitializeComponent();
            InitializeNavigation();
			NavigationPage.SetHasNavigationBar(this, false);

        }

        private void InitializeNavigation()
        {
            this.PageTitle = "View 4";
            this.RightIcon2 = "Close";
            this.RightButton2Command = new Command((obj) => Navigation.PopModalAsync());
        }
    }
}
