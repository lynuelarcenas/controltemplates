﻿using System;
using System.Collections.Generic;
using ControlTemplates.Utilities;
using Xamarin.Forms;

namespace ControlTemplates
{
    public partial class PageThree : NavigationButtons
    {
        public PageThree()
        {
            InitializeComponent();
            InitiliazeNavigation();
			NavigationPage.SetHasNavigationBar(this, false);
        }

        private void InitiliazeNavigation()
        {
            this.PageTitle = "View 3";
            this.LeftIcon = "BackArrow";
            this.RightIcon1 = "Edit";
            this.RightIcon2 = "Delete";

            this.LeftButtonCommand = new Command((obj) => Navigation.PopAsync());
            this.RightButton1Command = new Command((obj) => DisplayAlert("Edit", "This button is for edit function", "Okay"));
            this.RightButton2Command = new Command((obj) => DisplayAlert("Delete", "This button is for delete function", "Okay"));
        }
    }
}
