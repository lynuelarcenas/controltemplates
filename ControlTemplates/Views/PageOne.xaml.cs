﻿using System;
using System.Collections.Generic;
using ControlTemplates.Utilities;
using Xamarin.Forms;

namespace ControlTemplates
{
    public partial class PageOne : NavigationButtons
    {
        public PageOne()
        {
            InitializeComponent();
            InitializeNavigation();
            NavigationPage.SetHasNavigationBar(this, false);
            //this.PageTitle = "View 1";
            //this.LeftIcon = "BackArrow";
            //this.LeftButtonCommand = new Command((obj) => Navigation.PopAsync());


        }
        private void InitializeNavigation()
        {
            this.PageTitle = "View 1";
            this.LeftIcon = "BackArrow";
            this.LeftButtonCommand = new Command((obj) => Navigation.PopAsync());

        }
    }
}
